use std::fmt::{Debug, Display};

#[derive(Debug)]
pub struct WordError<T>(T);

impl From<&str> for WordError<String> {
    fn from(err: &str) -> Self {
        WordError(err.to_string())
    }
}

impl<T> From<T> for WordError<T> {
    fn from(err: T) -> Self {
        WordError(err)
    }
}

impl<T: Debug + Display> std::error::Error for WordError<T> {}

impl<T: Display> Display for WordError<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            WordError(err) => write!(f, "{}", err),
        }
    }
}
