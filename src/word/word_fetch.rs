extern crate hyper;
extern crate hyper_tls;
extern crate rand;

use hyper::{body::HttpBody as _, Client};
use rand::seq::SliceRandom;
use tokio::io::{self, AsyncWriteExt as _};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

pub struct Fetcher {
    word: String,
    random_chars: RandomChars,
}

struct RandomChars(char, char);

impl Fetcher {
    pub fn new() -> Fetcher {
        Fetcher {
            word: String::new(),
            random_chars: Fetcher::get_random_letters(),
        }
    }

    pub fn fetch(&mut self) -> &'static str {
        "test"
    }

    fn get_random_letters() -> RandomChars {
        let alphabet: [char; 26] = [
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
            'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        ];
        let mut rng = rand::thread_rng();
        let e_msg = "Could not get random chars";
        let mut random_chars = alphabet.choose_multiple(&mut rng, 2);
        RandomChars(
            *random_chars.next().expect(e_msg),
            *random_chars.next().expect(e_msg),
        )
    }

    #[tokio::main]
    async fn get_word(&self) -> Result<()> {
        // let RandomChars(start, end) = self.random_chars;
        let url = format!(
            // "https://wordsapiv1.p.mashape.com/words/?letterPattern=^{}.+{}$",
            "https://httpbin.org/ip"
            // start, end
        );

        // HTTPS requires picking a TLS implementation, so give a better
        // warning if the user tries to request an 'https' URL.
        let url = url.parse::<hyper::Uri>().unwrap();
        Fetcher::fetch_url(url).await
    }

    async fn fetch_url(url: hyper::Uri) -> Result<()> {
        let https = hyper_tls::HttpsConnector::new();
        let client = Client::builder().build::<_, hyper::Body>(https);
        let mut res = client.get(url).await?;
        println!("Response: {}", res.status());
        println!("Headers: {:#?}\n", res.headers());

        // Stream the body, writing each chunk to stdout as we get it
        // (instead of buffering and printing at the end).
        while let Some(next) = res.data().await {
            let chunk = next?;
            io::stdout().write_all(&chunk).await?;
        }
        println!("\n\nDone!");
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_random_chars() {
        let random_chars = Fetcher::get_random_letters();
        println!("{}, {}", random_chars.0, random_chars.1);
    }

    #[test]
    fn mock_fetch() {
        let f = Fetcher::new();
        f.get_word().unwrap();
    }
}
