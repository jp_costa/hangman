mod player_errors;
pub use player_errors::*;

pub struct Player {
    tries: i32,
    guessed_letters: Vec<char>,
}

impl Player {
    pub fn new() -> Player {
        Player {
            tries: 5,
            guessed_letters: Vec::new(),
        }
    }

    pub fn get_tries(&self) -> Result<&i32, PlayerError> {
        if self.tries <= 0 {
            return Err(PlayerError::NoMoreTries);
        }
        Ok(&self.tries)
    }

    pub fn get_guesses(&self) -> &[char] {
        &self.guessed_letters[..]
    }

    pub fn decrease_tries(&mut self) {
        self.tries -= 1;
    }

    pub fn make_a_guess(
        &mut self,
        usr_input: Result<String, std::io::Error>,
    ) -> Result<(), PlayerError> {
        let guess: Option<char> = match usr_input {
            Ok(s) => s.chars().nth(0),
            Err(io_err) => {
                return Err(PlayerError::from(InvalidGuesses::from(io_err)));
            }
        };
        if self.validate_guess(&guess)? {
            self.guessed_letters
                .push(guess.expect("Could not get guess"));
        }
        Ok(())
    }

    fn validate_guess(&self, &guess: &Option<char>) -> Result<bool, PlayerError> {
        match guess {
            Some(c) => {
                Player::check_if_is_alphabetic(&c)?;
                self.check_has_been_guessed(&c)?;
            }
            None => return Err(PlayerError::from(InvalidGuesses::EmptyGuess)),
        }
        Ok(true)
    }

    fn check_if_is_alphabetic(guess: &char) -> Result<(), PlayerError> {
        if guess.is_alphabetic() == false {
            return Err(PlayerError::from(InvalidGuesses::NotAlphabetic));
        }
        Ok(())
    }

    fn check_has_been_guessed(&self, guess: &char) -> Result<(), PlayerError> {
        if self.guessed_letters.is_empty() == false {
            if self
                .get_guesses()
                .iter()
                .any(|guessed_c| guessed_c == guess)
            {
                return Err(PlayerError::from(InvalidGuesses::AlreadyGuessed));
            }
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn check_player_creation() {
        let p = Player::new();
        assert_eq!(5, p.tries);
        assert!(p.get_guesses().is_empty());
    }

    // #[test]
    // fn check_decrease_tries() {
    //     let mut p = Player::new();
    //     for i in 0..4 {
    //         let msg = format!("{} {}", i, p.tries);
    //         p.decrease_tries().expect(&msg);
    //     }
    //     let result = p.decrease_tries();
    //     assert!(result.is_err());
    // }

    #[test]
    fn mock_a_guess() {
        let mut p = Player::new();
        assert!(p.make_a_guess(Ok("agjsghshrgks\n".to_string())).is_ok());
        assert_eq!(Some(&'a'), p.get_guesses().last());
    }

    #[test]
    fn mock_invalid_guesses() {
        let mut p = Player::new();
        assert!(p.make_a_guess(Ok("test".to_string())).is_ok());
        match_invalid_guesses(
            p.make_a_guess(Ok("1".to_string())),
            InvalidGuesses::NotAlphabetic,
        );
        match_invalid_guesses(
            p.make_a_guess(Ok("".to_string())),
            InvalidGuesses::EmptyGuess,
        );
    }

    fn match_invalid_guesses(error: Result<(), PlayerError>, invalid_guess: InvalidGuesses) {
        if let Err(e) = error {
            let e_msg = format!("{}", e);
            let i_err = format!("{}", invalid_guess);
            assert_eq!(e_msg, i_err);
        }
    }
}
