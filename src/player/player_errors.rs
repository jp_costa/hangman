#[derive(Debug)]
pub enum PlayerError {
    NoMoreTries,
    InvalidGuess(InvalidGuesses),
}

impl std::error::Error for PlayerError {}

impl From<InvalidGuesses> for PlayerError {
    fn from(err: InvalidGuesses) -> Self {
        PlayerError::InvalidGuess(err)
    }
}

impl std::fmt::Display for PlayerError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            PlayerError::InvalidGuess(err) => write!(f, "{}", err),
            PlayerError::NoMoreTries => write!(f, "no more tries"),
        }
    }
}

#[allow(dead_code)]
#[derive(Debug)]
pub enum InvalidGuesses {
    NotAlphabetic,
    AlreadyGuessed,
    EmptyGuess,
    IoError(std::io::Error),
}

impl From<std::io::Error> for InvalidGuesses {
    fn from(io_err: std::io::Error) -> Self {
        InvalidGuesses::IoError(io_err)
    }
}

impl std::fmt::Display for InvalidGuesses {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            InvalidGuesses::AlreadyGuessed => write!(f, "already guessed"),
            InvalidGuesses::EmptyGuess => write!(f, "empty guess"),
            InvalidGuesses::NotAlphabetic => write!(f, "not alphabetic"),
            InvalidGuesses::IoError(io_err) => write!(f, "{}", io_err),
        }
    }
}
