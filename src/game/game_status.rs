use crate::game::Game;

#[derive(Debug, PartialEq)]
pub enum GameStatus {
    Won,
    Over,
    Running,
}

impl GameStatus {
    pub fn update_with(game: &Game) -> GameStatus {
        if let Err(_) = game.player.get_tries() {
            return GameStatus::Over;
        } else if game.check_if_guessed_word() {
            return GameStatus::Won;
        }
        GameStatus::Running
    }

    pub fn check(game: &Game) -> Option<&str> {
        match *game.get_status() {
            GameStatus::Over => Some("GAME OVER!"),
            GameStatus::Running => None,
            GameStatus::Won => Some("YOU WON!"),
        }
    }
}
