use super::Game;
use crate::player::PlayerError;
use crate::word::WordError;

#[derive(Debug)]
pub struct GameError {
    kind: ErrorKind,
}

impl GameError {
    pub fn handle(g_err: Self, game: &mut Game) {
        match g_err.kind {
            ErrorKind::WordError(w_err) => {
                let e = format!("{}", w_err);
                if e == "no matches found" {
                    game.process_wrong_guess();
                }
            }
            ErrorKind::PlayerError(p_err) => {
                let p = format!("{}", p_err);
                eprintln!("{}", p);
            }
        }
    }
}

impl From<PlayerError> for GameError {
    fn from(err: PlayerError) -> Self {
        GameError {
            kind: ErrorKind::PlayerError(err),
        }
    }
}

impl From<WordError<String>> for GameError {
    fn from(err: WordError<String>) -> Self {
        GameError {
            kind: ErrorKind::WordError(err),
        }
    }
}

impl std::error::Error for GameError {}

impl std::fmt::Display for GameError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.kind)
    }
}

#[derive(Debug)]
pub enum ErrorKind {
    WordError(WordError<String>),
    PlayerError(PlayerError),
}

impl std::fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            ErrorKind::PlayerError(p_err) => write!(f, "{}", p_err),
            ErrorKind::WordError(w_err) => write!(f, "{}", w_err),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::player::{InvalidGuesses, PlayerError};

    #[test]
    fn check_player_errors() {
        let mut p_err = GameError::from(PlayerError::NoMoreTries);
        let mut e_msg = format!("{}", p_err);
        assert_eq!("no more tries".to_string(), e_msg);
        p_err = GameError::from(PlayerError::InvalidGuess(InvalidGuesses::AlreadyGuessed));
        e_msg = format!("{}", p_err);
        assert_eq!("already guessed", e_msg);
    }
}
