mod word_errors;
mod word_fetch;

pub use word_errors::WordError;
use word_fetch::Fetcher;
pub struct Word;

pub struct GuessedWordUpdate<'a> {
    pub word: &'a str,
    pub guessed_word: &'a str,
    pub guess: &'a char,
}

impl Word {
    pub fn new() -> &'static str {
        // "test"
        let mut fetcher = Fetcher::new();
        fetcher.fetch()
    }

    pub fn get_new_guessed_word(word: &str) -> String {
        let mut s = String::new();
        let guessed_chars = vec!['_'; word.len()];
        for c in guessed_chars {
            s = format!("{}{}", s, c);
        }
        s
    }

    pub fn update_guessed_word_with(gwu: GuessedWordUpdate) -> Result<String, WordError<String>> {
        let GuessedWordUpdate {
            word,
            guessed_word,
            guess,
        } = gwu;
        if !word.contains(*guess) {
            return Err(WordError::from("no matches found"));
        }
        let word = word.as_bytes();
        let mut letters: Vec<char> = Vec::new();
        for (i, mut c) in guessed_word.char_indices() {
            if c == '_' && word[i] == (*guess as u8) {
                c = *guess;
            }
            letters.push(c);
        }
        Ok(letters.into_iter().collect())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn mock_get_guessed_word() {
        let s = Word::get_new_guessed_word("test");
        assert_eq!("____", s);
    }

    #[test]
    fn mock_update_guessed_word() {
        assert_eq!(
            "te_t",
            Word::update_guessed_word_with(GuessedWordUpdate {
                word: "test",
                guessed_word: "t__t",
                guess: &'e',
            })
            .unwrap()
        );
    }
}
