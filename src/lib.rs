mod game;
mod player;
mod word;

use crate::game::{Game, GameStatus};
pub struct Hangman {
    game: Game,
}

impl Hangman {
    pub fn new() -> Hangman {
        Hangman { game: Game::new() }
    }

    pub fn run(&mut self) {
        while self.check_status() {
            self.game.run();
        }
    }

    fn check_status(&self) -> bool {
        self.game.get_status() == &GameStatus::Running
    }
}

#[cfg(test)]
mod tests {
    use super::Hangman;
    #[test]
    fn mock_game_flow() {
        let mut hangman = Hangman::new();
        hangman.run();
    }
}
