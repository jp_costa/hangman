mod game_errors;
mod game_status;

use crate::player::*;
use crate::word::*;
use game_errors::GameError;
pub use game_status::GameStatus;

pub struct Game {
    player: Player,
    word: &'static str,
    guessed_word: String,
    status: GameStatus,
}

impl Game {
    pub fn new() -> Game {
        let word = Word::new();
        let guessed_word = Word::get_new_guessed_word(&word);
        Game {
            player: Player::new(),
            status: GameStatus::Running,
            word,
            guessed_word,
        }
    }

    pub fn process_wrong_guess(&mut self) {
        println!("Ops! Wrong guess");
        self.player.decrease_tries();
        match self.player.get_tries() {
            Ok(tries) => println!("{} tries remaining", tries),
            Err(e) => {
                let g_err = GameError::from(e);
                GameError::handle(g_err, self);
            }
        }
    }

    pub fn get_status(&self) -> &GameStatus {
        &self.status
    }

    pub fn update_status(&mut self) {
        self.status = GameStatus::update_with(self);
        if let Some(s) = GameStatus::check(self) {
            print!("{}. ", s);
            println!("The word was \"{}\"", self.word);
        }
    }

    pub fn run(&mut self) {
        self.display_guessed_word();
        println!("-> Make a guess!");
        let usr_input = Game::get_usr_input();
        match self.player.make_a_guess(usr_input) {
            Ok(_) => self.process_last_guess(),
            Err(e) => {
                let g_err = GameError::from(e);
                GameError::handle(g_err, self);
            }
        }
        self.update_status();
    }

    fn display_guessed_word(&self) {
        let mut s = String::new();
        for letter in self.guessed_word.chars() {
            s = format!("{}{} ", s, letter);
        }
        println!("\t{}\t", s);
    }

    fn get_usr_input() -> Result<String, std::io::Error> {
        let mut s = String::new();
        std::io::stdin().read_line(&mut s)?;
        Ok(s)
    }

    fn process_last_guess(&mut self) {
        let last_guess = self
            .player
            .get_guesses()
            .last()
            .expect("Could not get last guess");
        println!("\tYou guessed: {}", last_guess);
        match Word::update_guessed_word_with(GuessedWordUpdate {
            word: self.word,
            guessed_word: self.guessed_word.as_str(),
            guess: last_guess,
        }) {
            Ok(s) => {
                println!("Nice guess");
                self.guessed_word = s;
            }
            Err(e) => {
                let g_err = GameError::from(e);
                GameError::handle(g_err, self);
            }
        }
    }

    pub fn check_if_guessed_word(&self) -> bool {
        if self.word == self.guessed_word {
            return true;
        }
        false
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_game_creation() {
        let g = Game::new();
        let w = Word::get_new_guessed_word("test");
        assert_eq!(GameStatus::Running, g.status);
        // Assuming that the default is test
        assert_eq!(w, g.guessed_word);
    }

    #[test]
    #[ignore = "fix word method calls"]
    fn check_making_guesses() {}

    #[test]
    fn check_word_guessing_check() {
        let mut g = Game::new();
        let guesses = ['t', 's', 'e'];
        for guess in guesses.iter() {
            g.guessed_word = Word::update_guessed_word_with(GuessedWordUpdate {
                word: "test",
                guessed_word: g.guessed_word.as_str(),
                guess,
            })
            .unwrap();
        }
        assert!(g.check_if_guessed_word());
    }
}
